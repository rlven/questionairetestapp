﻿using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Repositories
{
    public class AnswerRepository : Repository<Answer, int>, IAnswerRepository
    {
        public AnswerRepository(AppDbContext context) : base(context)
        {

        }
    }
}
