﻿using Domain.Interfaces.Repositories;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IUnitOfWork
    {
        IQuestionRepository QuestionRepository { get; }
        IAnswerRepository AnswerRepository { get; }
        void Save();
        Task SaveAsync();
    }
}
