﻿using Domain.Entities;
using Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services.Interfaces
{
    public interface IQuestionaireService
    {
        IEnumerable<Question> GetAllQuestions();
        Task SaveAnsersAsync(IEnumerable<AnswerViewModel> answers);
    }
}
